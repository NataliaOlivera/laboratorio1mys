
package ModelosySimulacion;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class Principal extends javax.swing.JFrame {
    DefaultTableModel modelo;
    
    
    public Principal() {
        initComponents();
        modelo = new DefaultTableModel();
        modelo.addColumn("Orden");
        modelo.addColumn("M");
        modelo.addColumn("M^2");
        modelo.addColumn("Digitos");
        modelo.addColumn("N");
        modelo.addColumn("Ui");
        jTableCompleta.setModel(modelo);
       
        jButtonPrueba.setEnabled(false);
        jButtonLimpiar.setEnabled(false);
//        modelo2 = new DefaultTableModel();
//        modelo.addColumn("Ui");
//        jTablaUi.setModel(modelo2);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanelInput = new javax.swing.JPanel();
        nro_ordenJlabel = new javax.swing.JLabel();
        semillaJlabel = new javax.swing.JLabel();
        jTextFieldOrden = new javax.swing.JTextField();
        jTextFieldSemilla = new javax.swing.JTextField();
        jButtonCalcular = new javax.swing.JButton();
        jButtonLimpiar = new javax.swing.JButton();
        jButtonPrueba = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableCompleta = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 566, Short.MAX_VALUE)
        );

        jLabel4.setFont(new java.awt.Font("Swis721 Ex BT", 2, 36)); // NOI18N
        jLabel4.setText("Generador de Numeros Aleatorios");

        jPanelInput.setBackground(new java.awt.Color(204, 204, 204));

        nro_ordenJlabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nro_ordenJlabel.setText("Orden:");

        semillaJlabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        semillaJlabel.setText("Semilla (M):");

        jButtonCalcular.setText("Calcular");
        jButtonCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCalcularActionPerformed(evt);
            }
        });

        jButtonLimpiar.setText("Limpiar");
        jButtonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLimpiarActionPerformed(evt);
            }
        });

        jButtonPrueba.setText("Prueba");
        jButtonPrueba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPruebaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelInputLayout = new javax.swing.GroupLayout(jPanelInput);
        jPanelInput.setLayout(jPanelInputLayout);
        jPanelInputLayout.setHorizontalGroup(
            jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInputLayout.createSequentialGroup()
                .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInputLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanelInputLayout.createSequentialGroup()
                                .addComponent(nro_ordenJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelInputLayout.createSequentialGroup()
                                .addComponent(semillaJlabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextFieldSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanelInputLayout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButtonLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButtonCalcular, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                .addComponent(jButtonPrueba, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelInputLayout.setVerticalGroup(
            jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInputLayout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(semillaJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nro_ordenJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldOrden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addComponent(jButtonCalcular, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonPrueba, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTableCompleta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTableCompleta);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanelInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 691, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(135, 135, 135))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 10, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(26, 26, 26))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCalcularActionPerformed
        jButtonPrueba.setEnabled(true);
        jButtonLimpiar.setEnabled(true);
        Integer orden = Integer.parseInt(jTextFieldOrden.getText()),j,digitoscs,indice_inicio;
        String semilla = jTextFieldSemilla.getText();
        Integer digitos_semilla = semilla.length(),n_int;
        Integer semilla_int;
        String datos[] = new String[6];
        long cuadrado_semilla_long;
         String cuadrado_semilla,n,ui;
         Double ui_dou;
        
        if(digitos_semilla >3){
        for(int i=0;i<orden;i++){
             j=i+1;
            datos[0] = j.toString();
            
            datos[1] = semilla;
            semilla_int= Integer.parseInt(semilla);
            cuadrado_semilla_long = (long)Math.pow(semilla_int, 2); 
            cuadrado_semilla =Long.toString((long) cuadrado_semilla_long);
            
            
            digitoscs = (Integer) cuadrado_semilla.length();
                if(digitos_semilla%2 ==0){
                    if(digitoscs%2 != 0){
                        cuadrado_semilla_long = cuadrado_semilla_long*10;
                        cuadrado_semilla =Long.toString((long) cuadrado_semilla_long);
                        
                    }
                }else{
                    if(digitoscs%2 == 0){
                    cuadrado_semilla_long = cuadrado_semilla_long*10;
                    cuadrado_semilla =Long.toString((long) cuadrado_semilla_long);
                   }
                }
                datos[2] = cuadrado_semilla;
            datos[3] = digitoscs.toString() ;
            
            indice_inicio=(cuadrado_semilla.length()-digitos_semilla)/2;
            n=cuadrado_semilla.substring(indice_inicio, indice_inicio+digitos_semilla);
            //n_int = Integer.parseInt(n);
            datos[4] = n;
            
            ui = "0."+n;
            datos[5] = ui;
            ui_dou= Double.parseDouble(ui);
            
            semilla = n;
  
            modelo.addRow(datos);

        }}else{
            JOptionPane.showMessageDialog(null, " El numero ingresado (semilla) debe tener mas de 3 digitos");
            jButtonLimpiarActionPerformed(evt);
        }
        
    }//GEN-LAST:event_jButtonCalcularActionPerformed

    private void jButtonPruebaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPruebaActionPerformed
       Ventana_comprobacion ventana2 = new Ventana_comprobacion();
       ventana2.setVisible(true);
       Double ui;
       Integer binaria=0;
       String datos[] = new String[2],ui_s;
       
       int cont=0;
        
        for(int i=0;  i<jTableCompleta.getRowCount();  i++){
            ui_s= jTableCompleta.getValueAt(i, 5).toString();
            ui =Double.parseDouble(ui_s) ;
            if (ui>=0 && ui<=0.5000) {
                     binaria=0;
                     cont=0;
                 }if(ui>0.5000 && ui<=1){
                     binaria=1;
//                     cont=cont+1;
//                      if(cont>1){
//                            corridas[0]=cont;
//                             ventana2.modeloFO.addRow(corridas);
//                     }
//                 corridas[0]=1;
//                 ventana2.modeloFO.addRow(corridas);
                }
                 
                
                 datos[0]=ui_s;
                 datos[1]=binaria.toString();
                 ventana2.modeloUi.addRow(datos);
                 
        }
        nuevaTabla();
        
            
        
    }//GEN-LAST:event_jButtonPruebaActionPerformed

    private void jButtonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLimpiarActionPerformed
        //Integer filas = Integer.parseInt(jTextFieldOrden.getText());
        jTextFieldOrden.setText("");
        jTextFieldSemilla.setText("");
        jTableCompleta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {"Orden", "M", "M^2", "Valida", "N", "Ui"}
        ));
        //Integer datos[] =new Integer[jTableCompleta.getRowCount()];
//        for(int i=0;  i<filas;  i++){
//
//            modelo.removeRow(i);
//        }
    }//GEN-LAST:event_jButtonLimpiarActionPerformed

public void nuevaTabla(){  
    modelo =new DefaultTableModel();
    jTableCompleta.setModel(modelo);
}
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCalcular;
    private javax.swing.JButton jButtonLimpiar;
    private javax.swing.JButton jButtonPrueba;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelInput;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableCompleta;
    private javax.swing.JTextField jTextFieldOrden;
    private javax.swing.JTextField jTextFieldSemilla;
    private javax.swing.JLabel nro_ordenJlabel;
    private javax.swing.JLabel semillaJlabel;
    // End of variables declaration//GEN-END:variables
}
