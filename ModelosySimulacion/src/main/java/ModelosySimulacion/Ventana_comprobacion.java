
package ModelosySimulacion;

import java.util.ArrayList;
import java.util.Collections;
import javax.swing.table.DefaultTableModel;

public class Ventana_comprobacion extends javax.swing.JFrame {
    public static DefaultTableModel modeloUi,modeloFrecuencia,modeloFO;
 
    public Ventana_comprobacion() {
        initComponents();
        modeloUi=new DefaultTableModel();
        modeloFrecuencia = new DefaultTableModel();
//        modeloFO = new DefaultTableModel();
        
        modeloUi.addColumn("Ui");
        modeloUi.addColumn("SI");
        jTableUi3.setModel(modeloUi);
       
        modeloFrecuencia.addColumn("FRECUENCIA OBSERVADA");
        modeloFrecuencia.addColumn("FRECUENCIA ESPERADA");
        modeloFrecuencia.addColumn("(FOi-FEi)^2/FEi");
        jTableFrecuencias.setModel(modeloFrecuencia);
        
        
//        modeloFO.addColumn("secuencias");
//        jTableFO.setModel(modeloFO);
        
    }

  

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableUi3 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableFrecuencias = new javax.swing.JTable();
        jTextFieldResultado = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabelResultado1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jtextresultado = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        jTableUi3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Ui"
            }
        ));
        jScrollPane2.setViewportView(jTableUi3);

        jTableFrecuencias.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jTableFrecuencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Frecuencia Observada", "Frecuencia Esperada", "(FO -FE)^2/FE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableFrecuencias.setRowHeight(30);
        jScrollPane3.setViewportView(jTableFrecuencias);

        jTextFieldResultado.setFont(new java.awt.Font("Cambria", 1, 24)); // NOI18N

        jLabelResultado1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelResultado1.setText("ESTADISTICO = 3.87");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel2.setText("Prueba Estadistica de Aleatoriedad ");

        jtextresultado.setBackground(new java.awt.Color(204, 204, 204));
        jtextresultado.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Resultado ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(557, 557, 557)
                                        .addComponent(jLabel1))
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 672, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jtextresultado, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel6)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jTextFieldResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(3, 3, 3)))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(403, 403, 403)
                                .addComponent(jLabelResultado1)))))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jTextFieldResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(jtextresultado, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(jLabelResultado1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1)))
                .addGap(14, 14, 14))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        int i=0;
        int j=0;
        Integer contador=0;
        Integer filas = jTableUi3.getRowCount(); 
        ArrayList<Integer> corridas = new ArrayList<Integer>();
        //Integer corrida[]= new Integer[1];
        
        while (i<filas) {            
            
            if(jTableUi3.getValueAt(i,1).equals("1")){
                j=i;
                while(jTableUi3.getValueAt(j,1).equals("1")){
                    contador = contador + 1;
                    j++;
                }
               //corrida[0]=contador;
               //modeloFO.addRow(corrida);
              corridas.add(contador);
              i=j+1;
              contador=0;
            }else{
                 i++;
            }        
        }


//Integer mayor = Collections.max(corridas);
//  Integer mayor = corridas.get(0); 
//    int mayor = (int) jTableFO.getValueAt(0,0);
//
//    for(int x=0;  x<jTableFO.getRowCount();  x++){      
//                if ((int) jTableFO.getValueAt(x,0) > mayor) {
//                        mayor = (int) jTableFO.getValueAt(x,0);
//                }
//        }
     
       Double frecuencia_esperada,paso4;
       int m=0;
       String datos[]= new String[3];
       Integer fo=0;
       Double resultado_sumatoria=0.0;
       Double estadistico=3.84;
   
       for(int t=0;  t<Collections.max(corridas);  t++){
            frecuencia_esperada=0.0;
            m=t+1;
            fo=Collections.frequency(corridas, m);
            frecuencia_esperada=(filas-m+3)/(double)Math.pow(2, m+1);
            paso4=(double)Math.pow((fo-frecuencia_esperada), 2)/frecuencia_esperada; //((FO-FE)^2)/FE paso 4 
            resultado_sumatoria = resultado_sumatoria+paso4;
            
            datos[0]= "FO"+m+": "+fo.toString();
            datos[1]=frecuencia_esperada.toString();
            datos[2]=paso4.toString();
            modeloFrecuencia.addRow(datos);
            
        }  
        String  string_resultado_sumatoria= String.valueOf(resultado_sumatoria );
         jTextFieldResultado.setText(string_resultado_sumatoria);
    
          if (resultado_sumatoria < estadistico) {
                        jtextresultado.setText("PASA LA PRUEBA");
                }else{
             jtextresultado.setText("NO PASA LA PRUEBA");
            }
          
    }//GEN-LAST:event_formWindowActivated
    
 
        
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana_comprobacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana_comprobacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana_comprobacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana_comprobacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana_comprobacion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelResultado1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTableFrecuencias;
    private javax.swing.JTable jTableUi3;
    private javax.swing.JTextField jTextFieldResultado;
    private javax.swing.JTextField jtextresultado;
    // End of variables declaration//GEN-END:variables
}
