
package laboratorio2;

import javax.swing.table.DefaultTableModel;

public class Ventana_Principal extends javax.swing.JFrame {
    DefaultTableModel modelo,modelo2;
    
    public Ventana_Principal() {

        initComponents();
        modelo = new DefaultTableModel();
        modelo.addColumn("DIA");
        modelo.addColumn("TOTAL OMNIBUS");
        modelo.addColumn("OMNIBUS PARA REPARACION");
        modelo.addColumn("GANANCIA ($)");
        jTableSemestre.setModel(modelo);
        
        modelo2=new DefaultTableModel();
        modelo2.addColumn("N° OMNIBUS");
        modelo2.addColumn("REPARACION");
        modelo2.addColumn("ABONO");
        jTable2.setModel(modelo2);
    }
    public static Integer abonoBase,m,d,incremento;
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButtonVerDia = new javax.swing.JButton();
        jButtonLimpiarDia = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldOmnisDiarios = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldOPR = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jTextFieldDia = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableSemestre = new javax.swing.JTable();
        jTextFieldUtilidadTotal = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldOm = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jButtonLimpiarTodo = new javax.swing.JButton();
        jButtonSimular = new javax.swing.JButton();
        jTextFieldcosto = new javax.swing.JTextField();
        jTextFieldabono = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldincremento = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFielddesvio = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldminimo = new javax.swing.JTextField();
        jTextFieldmaximo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setText("Simulador Semestral Empresa de Servicio Tecnico");

        jButtonVerDia.setText("Ver Detalle");
        jButtonVerDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVerDiaActionPerformed(evt);
            }
        });

        jButtonLimpiarDia.setText("Limpiar");
        jButtonLimpiarDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLimpiarDiaActionPerformed(evt);
            }
        });

        jLabel12.setText("Ingrese numero del dia para ver detalles ");

        jLabel8.setText("Cantidad de Omnibus que llegaron:");

        jTextFieldOmnisDiarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldOmnisDiariosActionPerformed(evt);
            }
        });

        jLabel11.setText("Cantidad de omnibus para reparacion:");

        jTextFieldOPR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldOPRActionPerformed(evt);
            }
        });

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Omnibus", "Reparacion", "Abono"
            }
        ));
        jScrollPane1.setViewportView(jTable2);

        jTextFieldDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDiaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldDia, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButtonVerDia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(4, 4, 4)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldOPR)
                            .addComponent(jTextFieldOmnisDiarios, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonLimpiarDia, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextFieldDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonVerDia))
                .addGap(34, 34, 34)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextFieldOmnisDiarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldOPR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonLimpiarDia)
                    .addComponent(jLabel11))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        jTableSemestre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Dia", "Omnibus que llegaron", "Omnibus Para Reparacion ", "Ganancia ($)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableSemestre);

        jTextFieldUtilidadTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldUtilidadTotalActionPerformed(evt);
            }
        });

        jLabel9.setText("Ganancia Total Semestre: $");

        jTextFieldOm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldOmActionPerformed(evt);
            }
        });

        jLabel10.setText("Total Omnibus:");

        jLabel13.setText("Datos Obtenidos en una simulacion de 182 dias");

        jButtonLimpiarTodo.setText("Limpiar");
        jButtonLimpiarTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLimpiarTodoActionPerformed(evt);
            }
        });

        jButtonSimular.setText("Simular");
        jButtonSimular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSimularActionPerformed(evt);
            }
        });

        jTextFieldcosto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldcostoActionPerformed(evt);
            }
        });

        jTextFieldabono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldabonoActionPerformed(evt);
            }
        });

        jLabel2.setText("Abono Vehiculo: $");

        jLabel5.setText("Costo promedio por materiales: $");

        jLabel3.setText("Incremento reparacion: $");

        jTextFieldincremento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldincrementoActionPerformed(evt);
            }
        });

        jLabel6.setText("Desvio (+/-): $");

        jTextFielddesvio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFielddesvioActionPerformed(evt);
            }
        });

        jLabel1.setText("Ingrese cantidad minima y maxima de vehiculos que ingresan por dia:");

        jTextFieldminimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldminimoActionPerformed(evt);
            }
        });

        jTextFieldmaximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldmaximoActionPerformed(evt);
            }
        });

        jLabel4.setText("Max:");

        jLabel14.setText("Min:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldOm, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(353, 353, 353)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldUtilidadTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addComponent(jLabel13)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldminimo, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldmaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 506, Short.MAX_VALUE)
                                .addComponent(jButtonLimpiarTodo, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldabono, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel5))
                                .addGap(1, 1, 1)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextFieldcosto, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                                    .addComponent(jTextFieldincremento))))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFielddesvio)
                                .addGap(2, 2, 2))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonSimular, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldminimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTextFieldmaximo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldabono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonLimpiarTodo))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldincremento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSimular))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldcosto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jTextFielddesvio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextFieldOm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jTextFieldUtilidadTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(367, 367, 367)
                        .addComponent(jLabel7)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(122, 122, 122)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1315, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 692, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldminimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldminimoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldminimoActionPerformed

    private void jTextFieldabonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldabonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldabonoActionPerformed

    private void jTextFieldincrementoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldincrementoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldincrementoActionPerformed

    private void jTextFieldcostoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldcostoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldcostoActionPerformed

    private void jTextFielddesvioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFielddesvioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFielddesvioActionPerformed

    private void jTextFieldmaximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldmaximoActionPerformed
        
    }//GEN-LAST:event_jTextFieldmaximoActionPerformed

    private void jButtonSimularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSimularActionPerformed
        //Inicializando variables :)
        long totalOmnisPorDia=0,OmnisTotalesSemestre=0,abono_vehiculo_reparacion=0,cm=0,ganancia_diaria_reparaciones=0,gR=0,gananciaDiaria=0,utilidadtotal=0,omnisParaMantenimiento=0;
        Integer cantidad_omnibus_ParaReparacionYMateriales=0,omnibus_ParaReparacionDia=0;
        //Tomando los datos de los campos
        Integer valor_minimo = Integer.parseInt(jTextFieldminimo.getText());
        Integer valor_maximo = Integer.parseInt(jTextFieldmaximo.getText());
        m=Integer.parseInt(jTextFieldcosto.getText());
        d= Integer.parseInt(jTextFielddesvio.getText());
        incremento=Integer.parseInt(jTextFieldincremento.getText());
        abonoBase = Integer.parseInt(jTextFieldabono.getText());
       
        
        
        
       
//       jTextFieldOD.setText(omnisDia.toString());
//       jTextFieldOR.setText(omnisDiaReparacion.toString());
       
      String datos[] = new String[4]; //Para llenar la tabla
      for(Integer i =1; i<=182;i++){
       Double u = Math.random();     
       totalOmnisPorDia = Math.round(valor_minimo+ (valor_maximo - valor_minimo)* u); //calculo omnibus que llegan por dia
       OmnisTotalesSemestre = OmnisTotalesSemestre + totalOmnisPorDia; // acumulador para cantidad de omnibus en el semestre
       omnibus_ParaReparacionDia= binomial(totalOmnisPorDia,0.300); // cantidad de omnibus que necesitan reparacion por dia, funcion binomial, envia: omnis totales por dia y probabilidad de que necesite reparacion
       
       for(int j=0;  j<omnibus_ParaReparacionDia;  j++){
           
           abono_vehiculo_reparacion = abono_vehiculo_reparacion +  incremento; // abono normal + incremento por reparacion
           cantidad_omnibus_ParaReparacionYMateriales=binomial(omnibus_ParaReparacionDia,0.500);//a cuantos omnis le cobramos materiales, ditribucion binomial le envio: 50% de probabilidad y la cantidad de autos que necesitan reparacion
           for(int k=0; k<cantidad_omnibus_ParaReparacionYMateriales;k++){
               cm=normal(m, d); //costo del material (distribucion normal) se envia la media y el desvio
               abono_vehiculo_reparacion = abono_vehiculo_reparacion + cm; // le sumo el costo del material al abono del vehiculo 
               ganancia_diaria_reparaciones = ganancia_diaria_reparaciones + abono_vehiculo_reparacion;// acumulador para la ganancia diaria de los autos reparados
           }
          
//        System.out.println("Abono Por Vehiculo a Reparar: " + abOR);

        }
       omnisParaMantenimiento = totalOmnisPorDia - omnibus_ParaReparacionDia; //Cantidad de autos que solo necesitan mantenimiento
       gananciaDiaria = (omnisParaMantenimiento *abonoBase) + ganancia_diaria_reparaciones;//ganancia total del dia
       utilidadtotal = gananciaDiaria + utilidadtotal;//acumulador para la ganancia semestral
       Long omnisDia= totalOmnisPorDia;//conversion por algun motivo que no recuerdo
       Long gananciadiaria = gananciaDiaria;
        
          datos[0]=i.toString();//se pone en la tabla
          datos[1]=omnisDia.toString();//se pone en la tabla
          datos[2]=omnibus_ParaReparacionDia.toString();//se pone en la tabla
          datos[3]="$"+gananciadiaria.toString();//se pone en la tabla
          modelo.addRow(datos); //se pone en la tabla
      }
      Long omnisTotal = OmnisTotalesSemestre;//conversion
      Long utilidadTotal = utilidadtotal;//conversion
      jTextFieldOm.setText(omnisTotal.toString());//Da el resultado en la pantalla de la cantidad de omnis en el semestre
      jTextFieldUtilidadTotal.setText(utilidadTotal.toString());//Resultado en pantalla de la ganancia total en el semestre
    }//GEN-LAST:event_jButtonSimularActionPerformed

    public void limpiarCeldasDia(){
    jTextFieldDia.setText("");
    jTextFieldOPR.setText("");
    jTextFieldOmnisDiarios.setText("");
    jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {"N° OMNIBUS","REPARACION","ABONO"}
        ));
    }
    public void limpiarCeldasSemestre(){
        jTextFieldminimo.setText("");
        jTextFieldmaximo.setText("");
        jTextFieldcosto.setText("");
        jTextFielddesvio.setText("");
        jTextFieldincremento.setText("");
        jTextFieldabono.setText("");
        for(int i=0; i<jTableSemestre.getRowCount();i++){
            modelo.removeRow(i);
        }
    }
     public static Integer binomial(long od, Double p){//parametros: cantidad de ensayos y probabilidad de que suceda un exito
        Integer contador=0;
        for(int i=0; i<od; i++){
            double u = Math.random(); //todo esto es de la teoria 
            if(u<p){
             contador++;
            }
        }
    return contador; //cantidad de exitos en los ensayos
}
    
    public static long normal(Integer media, Integer desvio){
        long costoMaterial=0;
        Double sum= 0.0;
        for(int i=0; i<12; i++){           //Mas cosas de la teoria
            double u = Math.random();
            sum=sum+u;
        }
    costoMaterial=Math.round(desvio*(sum-6)+media);
    return costoMaterial;
}
    
    private void jTextFieldOmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldOmActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldOmActionPerformed

    private void jTextFieldUtilidadTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldUtilidadTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldUtilidadTotalActionPerformed

    private void jButtonVerDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVerDiaActionPerformed

      String campo = jTextFieldDia.getText();
      Integer dia= Integer.parseInt(campo);
      
      jTextFieldOmnisDiarios.setText((String) jTableSemestre.getValueAt(dia-1, 1));
      jTextFieldOPR.setText((String) jTableSemestre.getValueAt(dia-1, 2));
     Integer omnisReparacion = Integer.parseInt(jTextFieldOPR.getText());
     Integer omnisTotal = Integer.parseInt(jTextFieldOmnisDiarios.getText());
     Integer omnisMantenimiento = omnisTotal - omnisReparacion;
      String datos[] = new String[3];
      Integer orym = binomial(omnisReparacion,0.500);
      Integer abono_vehiculo_reparacion = abonoBase + incremento;
      Integer i=1,j=1,k=1;
        while(i<=omnisTotal){ 
            while(j<=omnisReparacion){
                 while(k<=orym){
                     datos[0]=k.toString();
                     datos[1]="SI";
                     Integer cm=(int)normal(m, d);
                     Integer abono = abono_vehiculo_reparacion + cm; 
                     datos[2]=" $"+abono.toString();
                     modelo2.addRow(datos);
                     k++;
                     j=k;
                 }
                datos[0]=j.toString();
                datos[1]="SI";
                datos[2]= " $"+abono_vehiculo_reparacion.toString();
                modelo2.addRow(datos);
                j++;
                i=j;
            }
            datos[0]=i.toString();
            datos[1]="NO";
            datos[2]= " $"+abonoBase.toString();
            modelo2.addRow(datos);
            i++;
        } 
    }//GEN-LAST:event_jButtonVerDiaActionPerformed

    private void jTextFieldDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDiaActionPerformed

    private void jTextFieldOmnisDiariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldOmnisDiariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldOmnisDiariosActionPerformed

    private void jTextFieldOPRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldOPRActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldOPRActionPerformed

    private void jButtonLimpiarDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLimpiarDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonLimpiarDiaActionPerformed

    private void jButtonLimpiarTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLimpiarTodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonLimpiarTodoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana_Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonLimpiarDia;
    private javax.swing.JButton jButtonLimpiarTodo;
    private javax.swing.JButton jButtonSimular;
    private javax.swing.JButton jButtonVerDia;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTableSemestre;
    private javax.swing.JTextField jTextFieldDia;
    public static javax.swing.JTextField jTextFieldOPR;
    private javax.swing.JTextField jTextFieldOm;
    public static javax.swing.JTextField jTextFieldOmnisDiarios;
    private javax.swing.JTextField jTextFieldUtilidadTotal;
    public static javax.swing.JTextField jTextFieldabono;
    private javax.swing.JTextField jTextFieldcosto;
    private javax.swing.JTextField jTextFielddesvio;
    private javax.swing.JTextField jTextFieldincremento;
    private javax.swing.JTextField jTextFieldmaximo;
    private javax.swing.JTextField jTextFieldminimo;
    // End of variables declaration//GEN-END:variables
}
